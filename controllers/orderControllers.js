// orderControllers.js


const bcrypt = require("bcrypt");

const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product")


/* LOGGED-IN USER - ORDER */
module.exports.order = async (req,res) =>{

	// Check Admin User
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden. No Purchase feature for Admin Account."});
	}

	// Initialize sub-array to use for user and product update
	let products = req.body.products

	// Update User Subdocument Array but get order id as output
	let orderId = await User.findById(req.user.id).then(async (user) => {

		// Get Price and Quantity to compute total Amount
		let totalAmount = 0
		let index = 0
		while(index < products.length){
			totalAmount += await Product.findById(products[index].productId).then(product => {

				return (product.price * products[index].quantity)
			})
			index++
		} 

		let newOrder = {
			totalAmount: totalAmount,
			products: req.body.products
		}

		user.orders.push(newOrder);

		let orders = user.orders
		let orderId = orders[orders.length-1].id

		return user.save().then(user => orderId).catch(err => err.message);
	})

	let isUserUpdated = !!orderId

	// Check if output order_id has value, if true then update is successful
	if(!isUserUpdated){
		return res.send({message: "User cannot be updated."});
	}

	// Initialize so variable can be used outside of while loop
	let isProductUpdated = true

	// Update each product in one order using while loop
	let index = 0
	while(index < products.length){
		isProductUpdated = await Product.findById(products[index].productId).then(product => {

			let newOrderbyUser = {
				orderId: orderId,
				userId: req.user.id,
				quantity: products[index].quantity
			}
			
			product.orders.push(newOrderbyUser);

			return product.save().then(product => true).catch(err => err.message);
		})
		index++
	} 

	// If false, send message
	if(!isProductUpdated){
		return res.send({message: "Products cannot be updated."});
	}

	// Both true would give successful transaction
	if(isUserUpdated && isProductUpdated) {
        return res.send({message:"Your Purchase was Successful!"})
    }
    
}


/* DETAILS OF ALL ORDERS - AUTHENTICATED USER ONLY */
module.exports.allOrdersAllUsers = (req,res)=>{

	let filterEmpty = (user) => {return user.orders.length !== 0}

	User.find({}).select("orders")
	.then(result => res.send(result.filter(filterEmpty)))
	.catch(error => res.send(error))
}


/* ALL ORDERS OF ALL USERS - ADMIN ONLY */
module.exports.getUserOrderDetails = (req,res)=>{

	User.findById(req.user.id).select("orders")
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


/* ORDER DETAILS SHOWING ALL PRODUCTS */
module.exports.getOrderDetails = (req,res)=>{


	User.findById(req.user.id,{_id:0,orders:1})
	.then(result => {

		let products = result.orders
		let index = products.map(function(orders) { 
			return orders.id; }).indexOf(req.params.orderId);

		return res.send({id:req.params.orderId,products:products[index].products})
	})
	.catch(error => res.send(error))
}

