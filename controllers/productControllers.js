// productControllers.js


const Product = require("../models/Product");


/* CREATE PRODUCT (ADMIN ONLY)*/
module.exports.addProduct = (req,res) => {

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


/* UPDATE PRODUCT INFO (ADMIN ONLY) */
module.exports.updateProduct = (req,res)=>{

	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


/* ARCHIVE PRODUCT (ADMIN ONLY)*/
module.exports.archiveProduct = (req,res) =>{

	let update = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


/* RETRIEVE ALL ACTIVE PRODUCTS */
module.exports.getActiveProducts = (req,res)=>{

	Product.find({isActive: true},{orders:0})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


/* RETRIEVE SINGLE PRODUCT */
module.exports.getSingleProduct = (req,res)=>{

	Product.findById(req.params.productId,{orders:0})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


/* ACTIVATE PRODUCT */
module.exports.activateProduct = (req,res) =>{

	let update = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}