// userControllers.js


const bcrypt = require("bcrypt");

const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product")


/* USER REGISTRATION WITH HASHED PASSWORD */
module.exports.registerUser = (req,res) => {

	const hashedPw = bcrypt.hashSync(req.body.password,10);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


/* USER AUTHENTICATION */
module.exports.loginUser = (req,res) => {

	User.findOne({email:req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send({message: "E-mail Not Registered."})
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});

			} else {
				return res.send({message: "Incorrect Password."});
			}

		}
	})
}


/* USER DETAILS */
module.exports.getUserDetails = (req,res) => {

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


/* CHANGE USER TO ADMIN (ADMIN ONLY) */
module.exports.promoteToAdmin = (req,res) =>{

	let update = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}