// User.js

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type:String,
		required: [true,"first name is required"]
	},
	lastName: {
		type:String,
		required: [true,"last name is required"]
	},
	email: {
		type:String,
		required: [true,"email is required"]
	},
	password: {
		type:String,
		required: [true,"password is required"]
	},
	mobileNo: {
		type:String,
		required: [true,"mobile number is required"]
	},
	isAdmin: {
		type:Boolean,
		default: false
	},
	orders: [

		{
			totalAmount:	{
				type:Number,
				required: [true,"total amount is required"]
			},
			purchasedOn:	{
				type:Date,
				default: new Date()
			},
			products:	[
				{
					productId:	{
						type:String,
						required: [true,"product id is required"]
					},
					quantity:	{
						type:Number,
						required: [true,"quantity is required"]
					}
				}
			]
		}
	]

})


module.exports = mongoose.model("User",userSchema);