// Product.js

const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type:String,
		required: [true,"name is required"]
	},
	description: {
		type:String,
		required: [true,"description is required"]
	},
	price: { 
		type:Number,
		required: [true,"price is required"]
	},
	isActive: {
		type:Boolean,
		default: true
	},
	createdOn: {
		type:Date,
		default: new Date()
	},
	orders: [

		{
			orderId: {
				type:String,
				required: [true,"order id is required"]
			},
			userId:	{
				type:String,
				required: [true,"user id is required"]
			},
			quantity: {
				type:Number,
				required: [true,"quantity is required"]
			},
			purchasedOn: {
				type:Date,
				default: new Date()
			}
		}
	]
	
})


module.exports = mongoose.model("Product",productSchema);