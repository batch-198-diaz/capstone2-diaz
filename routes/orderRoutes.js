// orderRoutes.js

/* INITIALIZATION */
const express = require("express");
const router = express.Router();

const orderControllers = require("../controllers/orderControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


/* ROUTES */

	// Order (Authenticated Non-Admin User) - POST
router.post("/",verify,orderControllers.order);


	// Show All Orders from All Users (Admin) - GET
router.get("/",verify,verifyAdmin,orderControllers.allOrdersAllUsers);

	
	// Show Orders of Authenticated User - GET
router.get("/userOrders",verify,orderControllers.getUserOrderDetails);


	//Show All Products for One Order - GET
router.get("/products/:orderId",verify,orderControllers.getOrderDetails)


module.exports = router;