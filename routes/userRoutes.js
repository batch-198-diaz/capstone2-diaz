// userRoutes.js

/* INITIALIZATION */
const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


/* ROUTES */

	//User Registratoin - POST
router.post("/",userControllers.registerUser);


	// User Authentication - POST
router.post("/login",userControllers.loginUser);

	
	// Show User Details - GET
router.get("/details",verify,userControllers.getUserDetails);


	// Update User to Admin - PUT
router.put("/admins/:userId",verify,verifyAdmin,userControllers.promoteToAdmin);


module.exports = router;