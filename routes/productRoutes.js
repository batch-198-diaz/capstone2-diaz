// productRoutes.js

/* INITIALIZATION */
const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


/* ROUTES */

	// Create Product - POST
router.post("/",verify,verifyAdmin,productControllers.addProduct);

	// Update Product - PUT
router.put("/:productId",verify,verifyAdmin,productControllers.updateProduct);

	// Archive Product - DELETE
router.delete("/archive/:productId",verify,verifyAdmin,productControllers.archiveProduct)

	// Show All Active Products - GET
router.get("/active",productControllers.getActiveProducts);

	// Show Single Product - GET
router.get("/details/:productId",productControllers.getSingleProduct);

	// Activate Product - PUT
router.put("/activate/:productId",verify,verifyAdmin,productControllers.activateProduct)	

module.exports = router;