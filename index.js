// index.js

const express = require("express");
const mongoose = require("mongoose");

const app = express();
app.use(express.json());

const port = 4000;

mongoose.connect("mongodb+srv://admin:admin123@cluster0.xxn22.mongodb.net/Ecommerce_API?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, "MongoDB Connection Error."));
db.once('open',()=>console.log("Connected to MongoDB"));


const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);

const orderRoutes = require('./routes/orderRoutes');
app.use('/orders',orderRoutes);


app.listen(port,()=>console.log("ExpressJS API running at localhost:4000"))